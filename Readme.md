# Hexagon Coding Challenge
To run, download the repo and run the index.html in a browser.

# Additional Challenges
* Make the hexagon animate.
    * Hexagon moves and rotates on screen
* Make a more complex gesture or animation.
    * Added background and mouse cursor animations
* Mask an image into the hexagon.
    * Hexagon drawn with a tiled image and a partially transparent random color
* Add some audio effects.
    * Clicking on the hexagon plays a sound
* Anything else you would like to add!
    * Hexagon has a health system. Clicking it enough times changes the hexagon to a triangle to 12-gon of random size.

# Notes
Written in TypeScript. Compiled with the ```tsc app.ts``` command.

No libraries were used for this project. It seemed more interesting to do things from scratch. I may have gone a bit far though...

Unfortunately at this point in time, I do not have a suitable mobile device to test it with, so the page's mobile performance is untested.
