//=====================================================================
// Main Hexagon and Update Loop
//=====================================================================

class Hexagon {
	x: number;
	y: number;
	width: number;
	height: number;
	angle: number;
	fillColor: string;
	strokeColor: string;
	lineWidth: number;
	hexagonPath: Path2D;
	animating: boolean;
	health: number;

	constructor(x: number, y: number, width: number, height: number, fillColor?: string, strokeColor?: string, lineWidth?: number) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.angle = 0;
		this.fillColor = fillColor || "#FFF";
		this.strokeColor = strokeColor || "#F0F";
		this.lineWidth = lineWidth || 5;
		this.hexagonPath = this.GenerateHexagon();
		this.animating = false;
		this.health = 3;
	}

	Draw(fillColor?: string, strokeColor?: string, lineWidth?: number) {
		ctx.save();

		//Draw Hexagon
		ctx.lineWidth = lineWidth || this.lineWidth;
		ctx.fillStyle = fillColor || this.fillColor;
		ctx.strokeStyle = strokeColor || this.strokeColor;
		ctx.translate(this.x, this.y);
		ctx.rotate(this.angle);
		ctx.stroke(this.hexagonPath);

		//Fill Hexagon
		ctx.save();

		//Draw bg as hexagon first layer
		const tempFillStyle = ctx.fillStyle;
		ctx.globalAlpha = 1;
		ctx.fillStyle = bgPattern;
		ctx.fill(this.hexagonPath);

		//Draw color as hexagon second layer
		ctx.globalAlpha = 0.75;
		ctx.fillStyle = tempFillStyle;
		ctx.fill(this.hexagonPath);
		ctx.restore();
		
		//Draw Text
		const text = this.health.toString();
		ctx.rotate(-this.angle);
		ctx.font = "100px Arial";
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.fillStyle = "#FFF";
		ctx.strokeStyle = "#000";
		ctx.lineWidth = 2;
		ctx.fillText(text, 0, 0);
		ctx.strokeText(text, 0, 0);

		ctx.restore();
	}

	private GenerateHexagon(edgeCount: number = 6) {
		const hexagonPath = new Path2D();
		for (let i = 0; i < edgeCount; i++) {
			hexagonPath.lineTo(Math.cos((PI2 * i) / edgeCount) * this.width/2, Math.sin((PI2 * i) / edgeCount) * this.height/2);
    }
		hexagonPath.closePath();
		return hexagonPath;
	}

	ChangeShape(edgeCount: number = 6){
		hexagon.hexagonPath = this.GenerateHexagon(edgeCount);
	}

	IsOverlapping(x?: number, y?: number){
		//The hexagon path is in local coordinates.
		//Transform it back to world coords first.
		ctx.save();
		ctx.translate(this.x, this.y);
		ctx.rotate(this.angle);
		let result = ctx.isPointInPath(hexagon.hexagonPath, x, y);
		ctx.restore();
		return result;
	}

	Damage(){
		this.health--;

		if(hitSound){
			hitSound.pause();
			hitSound.currentTime = 0;
			hitSound.play();
		}

		if (this.health <= 0) {
			this.animating = true;

			this.health = 3;
			this.width = RandomRange(20, 250);
			this.height = RandomRange(50, 350);
			this.ChangeShape(RandomRange(3, 12));

			this.animating = false;
    }
		this.fillColor = RandomColor();
	}
}

function Update(timestamp: number){
	requestAnimationFrame(Update);

	//Get a timestamp for animations.
	const time = timestamp / 1000;

	//Resize the canvas to fit the screen
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	//Clear the canvas
	ctx.fillStyle = `hsl(${(time * 30) % 360}, 50%, 10%)`;
	ctx.fillRect(0, 0, canvas.width, canvas.height);

	//Draw Back Ground
	DrawBackGround(time);

	//Debug Text
	ctx.fillStyle = "#FFF";
	ctx.fillText(mousePosition.toString(), 15, 15);

	//Animate Hexagon (Spin and rotate)
	hexagon.x = canvas.width / 2 + Math.cos(time) * 100;
	hexagon.y = canvas.height / 2 + Math.sin(time) * 100;
	hexagon.angle = (hexagon.angle + 2 * deg2rad) % PI2;

	//Draw Hexagon. Color the outline.
	hexagon.strokeColor = hexagon.IsOverlapping(...mousePosition) ? "#F00" : "#F0F";
	hexagon.Draw();

	//Draw Cursor
	DrawCursor(time);

	//Highlight hexagon
	HighlightHexagon();
}

//=====================================================================
// Helper Functions
//=====================================================================

function HighlightHexagon() {
	ctx.save();
	if (hexagon.IsOverlapping(...mousePosition)) {
		highlighted = highlighted + 0.05;
		if(highlighted >= 1){
			highlighted = 1;
		}

		ctx.lineWidth = 1;
		ctx.strokeStyle = "#0F0";
		const maxDimension = Math.max(hexagon.width, hexagon.height);
		const maxSize = Lerp(1000, maxDimension, highlighted);

		ctx.translate(hexagon.x, hexagon.y);
		ctx.rotate(Lerp(Math.PI / 2, 0, highlighted));
		ctx.strokeRect(-maxSize / 2, -maxSize / 2, maxSize, maxSize);
	}
	else{
		highlighted = 0;
	}
	ctx.restore();
}

function DrawCursor(time: number) {
	ctx.save();
	ctx.strokeStyle = "#0F0";
	ctx.lineWidth = 1;
	ctx.translate(mousePosition[0], mousePosition[1]);
	let size = 25;
	ctx.strokeRect(-size / 2, -size / 2, size, size);
	size = 40;
	ctx.rotate(-time);
	ctx.strokeRect(-size / 2, -size / 2, size, size);
	size = 15;
	ctx.rotate(time * 5);
	ctx.strokeRect(-size / 2, -size / 2, size, size);
	ctx.restore();
}

function DrawBackGround(time: number) {
	ctx.save();

	ctx.globalAlpha = Math.abs(Math.sin(time * 0.25) * 0.4);
	ctx.translate(canvas.width / 2, canvas.height / 2);
	ctx.scale(1 + (Math.sin(time * 0.2) * 0.25), 1 + ((Math.sin(time * 0.4) * 0.25)));
	ctx.rotate(time * 0.1);
	ctx.fillStyle = bgPattern;
	const maxSize = Math.max(canvas.width, canvas.height);
	ctx.fillRect(-maxSize, -maxSize, maxSize * 2, maxSize * 2);
	
	ctx.restore();
}

function RandomColor(){
	return `rgb(
		${RandomRange(0, 255)},
		${RandomRange(0, 255)},
		${RandomRange(0, 255)})`
}

function RandomRange(min: number, max: number){
	return Lerp(min, max, Math.random());
}

function Lerp(a: number, b: number, t: number){
	return a * (1 - t) + b * t;
}

//=====================================================================
// Initialization
//=====================================================================

//Get the canvas and context
const canvas = document.getElementById("hex-canvas") as HTMLCanvasElement;
if (!canvas) console.error("No canvas found...");
const ctx = canvas.getContext("2d");
if (!ctx) console.error("2D context failure...");

//Some useful constants
let mousePosition = [-1, -1];
const PI2 = Math.PI * 2;
const deg2rad = Math.PI / 180;

//How long the hexagon was highlighted.
let highlighted = 0;

//Create hexagon object
const hexagon = new Hexagon(canvas.width / 2, canvas.height / 2, 150, 150);

//Mouse position listener
canvas.addEventListener("mousemove", function(ev){
	mousePosition = [ev.clientX - canvas.offsetLeft, ev.clientY - canvas.offsetTop];
});

//Double click listener. Much nicer than the dblclick event.
let clickCount = 0;
let clickTimeout;
canvas.addEventListener("click", function(ev){
	if (!hexagon.animating && hexagon.IsOverlapping(...mousePosition)) {
		clickCount++;

		//First click
		if(clickCount === 1){
			//Cancel the dblclick timer with timeout if too late.
			clickTimeout = setTimeout(() => {
				clickCount = 0;
			}, 250);
		}
		//Second click
		else if(clickCount === 2){
			//The dblclick was successful
			clickCount = 0;
			clearTimeout(clickTimeout);

			//Fire the dblclick event here
			hexagon.Damage();
		}
	}
});

//Sound effect
const hitSound = document.getElementById("hit-sound") as HTMLAudioElement;
if(hitSound){
	hitSound.volume = 0.5;
}

//Create a pattern from some image
let bgPattern;
const bg = new Image();
bg.onload = function(){
	bgPattern = ctx.createPattern(bg, "repeat");
	
	//Start the rendering
	Update(0);
}
bg.src = "bg.png";


